# Bastille MySQL

---

#### Arguments

	VERSION    default value 57
	USER	   default value cead
	PWD        default value ADC987#
	DB         default value wiki


---

#### How to use this

```bash
[root@machine ~/ ] #: bastille template [jail] https://gitlab.com/fredericosales/bastille-mysql
```

---

#### Change some arguments

```bash
[root@machine ~/ ] #: bastille template [jail] https://gitlab.com/fredericosales/bastille-mysql --arg VERSION=8
```

---